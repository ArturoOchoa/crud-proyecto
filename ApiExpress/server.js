const express =  require('express');
const app = express();
const port = 8000;

app.listen(port, function(err) {
    if(err){
        console.log("error creando el servidor");
    }
    else{
        console.log("servidor iniciado correctamente en el puerto " + port);
    }
})

app.get('/', function(req,res){

    res.send('hola mundo de las apis')
})