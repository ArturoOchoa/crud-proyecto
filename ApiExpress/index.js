const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

app.get('/', function (req,res){
    res.send('Sensores Api');
});

app.get('/Sensores', function (req,res){
    var dato = {id:1, Type:'Temperatura', grados : 38, min:200};
    res.send(dato);
})

app.post('/Sensores', function(req,res){
    mens = {mensaje :'mensaje creado correctamente'};
    console.log(req.body);
    if(!req.body.id || !req.body.nombre){
        mens = {mensaje : 'error, Tipo de sensor'};
    }
    res.status(201);
    res.send(mens);
});


app.put('/Sensores/:id', function(req,res){
    res.send(' mensaje actualizado ');
    if(!req.body.id || !req.body.nombre){
        mens = {mensaje : 'error, Al Actualizar'};
    }
})

app.delete('/Sensores/:id', function(req,res){
    res.send('mensaje eliminado');
    if(!req.body.id || !req.body.nombre){
        mens = {mensaje : 'error, Al eliminar'};
    }

})

app.listen(8001, () => {
    console.log('El servidor esta inicializado en el puerto 8001')
});